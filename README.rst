=======================
ansible-role-gitinstall
=======================

Clone a git repository and make install.

Very suitable for a GNU stow compatible git repository of dotfiles such as https://gitlab.com/jfriis/dotfiles

Requirements
============

Your ansible must have
- `community.general`
- `ansible.posix`

.. code::

   ansible-galaxy collection install community.general ansible.posix

The remote machine must have git.

The repository must support GNU Make

.. code::

    make

If the repository requires authentication an SSH key can be installed using for instance https://github.com/Oefenweb/ansible-ssh-keys

If the SSH key requires a passphrase you can add the following to your ansible connection e.g. :code:`ansible.cfg` to forward an SSH agent:

.. code:: ini

   [ssh_connection]
   ssh_args = -o ControlMaster=auto -o ControlPersist=60s -o ControlPath=/tmp/ansible-ssh-%h-%p-%r -o ForwardAgent=yes

But then you should not set the :code:`ssh_key` variable..

Variables
=========

.. code:: yaml

   gitinstalls: []
   # - repo: https://github.com/webpro/dotfiles.git
   #   version: "{{ gitinstall_default_version }}"
   #   ssh_key: "{{ gitinstall_default_ssh_key }}"
   #   path: "{{ gitinstall_default_root_path }}/{{ item.repo.split('/')[-1].split('.')[0] }}"
   #   temporary: "{{ gitinstall_default_temporary }}"
   gitinstall_default_version: master
   gitinstall_default_ssh_key: ~/.ssh/id_rsa
   gitinstall_default_root_path: '~'
   gitinstall_default_temporary: false
   gitinstall_default_remote: true
   gitinstall_default_make: true  # whether to run make at all


Set :code:`gitinstall_default_remote=false` or :code:`item.remote=true` to clone on your local machine and transfer an archive of it.
Otherwise, the role clones from the remote node which may complicate authentication for private repos with SSH passphrases.

If you use a passhprase-less SSH key you can use it by :code:`item.key_file=~/.ssh/id_rsa`.
If you use a passphrase you should add the key to your SSH agent by :code:`ssh-add`.


Example Playbook
================

.. code:: yaml

    - hosts: all
      roles:
         - gitinstall


License
=======

GPL-3.0-only
